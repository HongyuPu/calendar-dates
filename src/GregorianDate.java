public class GregorianDate {
    private int year; //final year variable
    private int month; //final month variable
    private int day; //final day variable

    public GregorianDate() { //default constructor
        this.year = 0;
        long remainderDays;
        remainderDays = ((System.currentTimeMillis() + java.util.TimeZone.getDefault().getRawOffset()) / 86400000);
        while (!isLeapYear(this.year) && remainderDays >= 365 || isLeapYear(this.year) && remainderDays >= 366) {
            if (!isLeapYear(this.year)) {
                remainderDays -= 365; //finds the current year by operating through days
                this.year++;
            } else {
                remainderDays -= 366;
                this.year++;
            }
        }
        this.year += 1970; //takes the system time and convertitng it into days than adds it to 1970.

        int monthCurrent = 1;
        for (int i = 1; remainderDays >= getNumberOfDaysInMonth(year, i) && i <= 12; i++) {
            remainderDays = remainderDays - getNumberOfDaysInMonth(year, i); //gets the number of months from remaining days
            monthCurrent++;
        }
        this.month = monthCurrent;

        this.day = (int) remainderDays + 1; //gets the current number of days in the month
    }

    public GregorianDate(int year, int month, int day) { //overloaded constructor
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public void subtractDays(int days) {
        int daysSubtracted;
        daysSubtracted = this.day - days;
        while (daysSubtracted < 1) {
            if (month - 1 < 1) {
                daysSubtracted += getNumberOfDaysInMonth(this.year, 12); //increments the year and resets the month
                this.month = 12;
                this.year--;
            }
            else {
                daysSubtracted += getNumberOfDaysInMonth(this.year, this.month - 1);
                this.month--; //increments the months
            }
        }
        this.day = daysSubtracted; //Sets final number of days
    }

    public void addDays(int days) {
        int daysAdded;
        daysAdded = this.day + days;
        while (daysAdded > getNumberOfDaysInMonth(this.year, this.month)) {
            daysAdded -= getNumberOfDaysInMonth(this.year, this.month); //increments the months
            month++;
            while (this.month > 12) { //increments the years
                this.month = 1;
                this.year++;
            }
        }
        this.day = daysAdded; //sets the number of days
    }

    public void printShortDate() {
        System.out.printf("%d/%d/%d", month, day, year);
    }

    public void printLongDate() {
        System.out.printf("%s %d, %d", getMonthName(month), day, year);
    }

    public int getCurrentYear() {
        return this.year;
    }

    public int getCurrentMonth() {
        return this.month;
    }

    public int getCurrentDayOfMonth() {
        return this.day;
    }

    public String getCurrentMonthName() {
        return getMonthName(this.month);
    } //uses te get month name method to find month name

    public boolean isLeapYear() {
        if (this.year % 4 == 0 && !(this.year % 100 == 0) || this.year % 400 == 0) {
            return true; //public leap year method for testing
        }
        else {
            return false;
        }
    }

    private boolean isLeapYear(int year) {
        if (this.year % 4 == 0 && !(this.year % 100 == 0) || this.year % 400 == 0) {
            return true; //leap year method for other methods
        }
        else {
            return false;
        }
    }

    private int getNumberOfDaysInMonth(int year, int month) {
        int numberOfDays = 0; //returns number of days in the month
        switch (month) {
            case 1:
                numberOfDays = 31;
                break;
            case 2:
                if (isLeapYear(year)) {
                    numberOfDays = 29;
                }
                else {
                    numberOfDays = 28;
                }
                break;
            case 3:
                numberOfDays =  31;
                break;
            case 4:
                numberOfDays = 30;
                break;
            case 5:
                numberOfDays = 31;
                break;
            case 6:
                numberOfDays = 30;
                break;
            case 7:
                numberOfDays = 31;
                break;
            case 8:
                numberOfDays = 31;
                break;
            case 9:
                numberOfDays = 30;
                break;
            case 10:
                numberOfDays = 31;
                break;
            case 11:
                numberOfDays = 30;
                break;
            case 12:
                numberOfDays = 31;
                break;
            default:
                break;
        }
        return numberOfDays;
    }

    private String getMonthName(int month) {
        String monthName = ""; //returns the name of the month
        switch (month) {
            case 1:
                monthName = "January";
                break;
            case 2:
                monthName = "February";
                break;
            case 3:
                monthName = "March";
                break;
            case 4:
                monthName = "April";
                break;
            case 5:
                monthName = "May";
                break;
            case 6:
                monthName = "June";
                break;
            case 7:
                monthName = "July";
                break;
            case 8:
                monthName = "August";
                break;
            case 9:
                monthName = "September";
                break;
            case 10:
                monthName = "October";
                break;
            case 11:
                monthName = "November";
                break;
            case 12:
                monthName = "December";
                break;
            default:
                break;
        }
        return monthName;
    }
}
